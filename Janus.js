import JanusSocket from './utils/JanusSocket';

export default class JanusMain {
    /**
     * @type {RTCSessionDescription}
     */
    static RTCSessionDescription;
    /**
     * @type {RTCPeerConnection}
     */
    static RTCPeerConnection;
    /**
     * @type {RTCIceCandidate}
     */
    static RTCIceCandidate;

    apiSecret = null;
    iceServers = [];


    constructor(address) {
        this.socket = new JanusSocket(address);
    }

    /**
     *
     * @param RTCSessionDescription
     * @param RTCPeerConnection
     * @param RTCIceCandidate
     * @param MediaStream
     */
    static setDependencies = ({RTCSessionDescription, RTCPeerConnection, RTCIceCandidate, MediaStream}) => {
        JanusMain.RTCSessionDescription = RTCSessionDescription;
        JanusMain.RTCPeerConnection = RTCPeerConnection;
        JanusMain.RTCIceCandidate = RTCIceCandidate;
        JanusMain.MediaStream = MediaStream;
    };

    init = async () => {
        try {
            await this.socket.connect();
        } catch (e) {
            console.error('connect janus', e);
        }
    };

    destroy = async () => {
        try {
            const destroySessionResponse = await this.socket.sendAsync({
                'janus': 'destroy',
                'session_id': this.socket.sessionID,
            });
        } catch (e) {
            console.error('destroy janus', e);
        }

        try {
            await this.socket.disconnect();

        } catch (e) {
            console.error('destroy socket', e);
        }
    };
    isConnect = () => {
       return this.socket.connected;
    };
    /**
     *
     * @param secret
     */
    setApiSecret = (secret) => {
        this.apiSecret = secret;
    };

    /**
     *
     * @param {Object[]} iceServers
     */
    setIceServers = (iceServers) => {
        this.iceServers = iceServers;
    };
}
